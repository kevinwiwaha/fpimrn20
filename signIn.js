import React, { Component } from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";

const mainApp = () => {
  return (
    <View>
      <View>
        <Text style={styles.register}>Welcome</Text>
        <Text style={styles.registerKecil}>Sign up to continue</Text>
      </View>
      <View style={styles.bordertxt}>
        <Text style={styles.txt}>Email</Text>
        <View style={styles.box}></View>
        <Text style={styles.txt}>Password</Text>
        <View style={styles.box}></View>
      </View>
      <View style={styles.container}>
        <TouchableOpacity style={styles.daftar}>
          <Text style={styles.dalamDaftar}>Sign In</Text>
        </TouchableOpacity>
        <Text style={styles.or}>-OR-</Text>
        <View style={styles.boxLogo}>
          <Text style={styles.logoContainer}>
            facebook <Text style={styles.logoContainer}>google</Text>
          </Text>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  register: {
    fontSize: 30,
    marginTop: 160,
    marginLeft: 22,
    color: "#003366",
  },
  registerKecil: {
    fontSize: 12,
    marginLeft: 22,
  },
  img: {
    alignItems: "center",
    marginTop: 63,
  },
  bordertxt: {
    marginLeft: 48,
    marginTop: 40,
  },
  txt: {
    fontSize: 12,
    color: "#003366",
  },
  box: {
    width: 294,
    height: 30,
    borderBottomWidth: 1,
    // borderBottomColor: "black",
    backgroundColor: "white",
  },
  daftar: {
    width: 318,
    height: 50,
    borderRadius: 5,
    backgroundColor: "#F77866",
    marginTop: 59,
  },
  dalamDaftar: {
    paddingTop: 9,
    textAlign: "center",
    color: "white",
  },
  teksAtau: {
    color: "#4D4D4D",
    paddingTop: 11,
  },
  masuk: {
    width: 140,
    height: 40,
    borderRadius: 10,
    backgroundColor: "#3EC6FF",
    marginTop: 16,
  },
  or: {
    marginTop: 31,
  },
  boxLogo: {
    marginLeft: 48,
    width: 318,
    height: 44,
  },
  logoContainer: {
    paddingLeft: 50,
  },
});
export default mainApp;
