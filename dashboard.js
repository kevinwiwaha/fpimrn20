import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { color } from "react-native-reanimated";

const dashboard = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.head}>Find Route</Text>
      <View style={{ height: 25 }}>
        <ScrollView horizontal>
          <View style={styles.tagSmall}>
            <Text style={styles.tagText}>West Java</Text>
          </View>
          <View style={styles.tagSmall}>
            <Text style={styles.tagText}>East Java</Text>
          </View>
          <View style={styles.tagSmall}>
            <Text style={styles.tagText}>Central Java</Text>
          </View>
          <View style={styles.tagSmall}>
            <Text style={styles.tagText}>Jogjakarta</Text>
          </View>
          <View style={styles.tagSmall}>
            <Text style={styles.tagText}>Jakarta</Text>
          </View>
        </ScrollView>
      </View>
      <View style={styles.searchBar}>
        <View style={styles.searchIcon}>
          <Icon name="search" size={9} color="#36609F" />
        </View>

        <TextInput style={styles.textBar}>Find a crew here...</TextInput>
      </View>

      <Text style={styles.navOrder}>Delivery Order</Text>
      <View
        style={({ justifyContent: "space-between" }, { flexDirection: "row" })}
      >
        <View style={styles.boxXL}></View>
        <View style={styles.boxL}></View>
      </View>
      <View
        style={({ justifyContent: "space-between" }, { flexDirection: "row" })}
      >
        <View style={styles.boxM1}></View>
        <View style={styles.boxM2}></View>
        <View style={styles.boxM3}></View>
      </View>
      <Text style={styles.NavRecent}>Recent Activity</Text>
      <Image
        style={styles.imageCheck}
        source={{
          uri:
            "https://www.figma.com/file/GllD5vJGv9pWfVEmpm0sIJ/Wartek?node-id=27%3A66",
        }}
      />
      <ScrollView>
        <View style={styles.activityBox}>
          <Text style={styles.activityText}>Package arrived in warehouse</Text>
          <Text style={styles.activityText2}>Surabaya, Indonesia</Text>
        </View>
        <View style={styles.activityBox}>
          <Text style={styles.activityText}>Package arrived in warehouse</Text>
          <Text style={styles.activityText2}>Surabaya, Indonesia</Text>
        </View>
        <View style={styles.activityBox}>
          <Text style={styles.activityText}>Package arrived in warehouse</Text>
          <Text style={styles.activityText2}>Surabaya, Indonesia</Text>
        </View>
        <View style={styles.activityBox}>
          <Text style={styles.activityText}>Package arrived in warehouse</Text>
          <Text style={styles.activityText2}>Surabaya, Indonesia</Text>
        </View>
        <View style={styles.activityBox}>
          <Text style={styles.activityText}>Package arrived in warehouse</Text>
          <Text style={styles.activityText2}>Surabaya, Indonesia</Text>
        </View>
      </ScrollView>

      {/* <Text style={styles.navOrder}>Delivery Order</Text>
      <Text style={styles.navOrder}>Delivery Order</Text>
      <Text style={styles.navOrder}>Delivery Order</Text> */}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 10,
    paddingLeft: 15,
    backgroundColor: "#E5E5E5",
  },
  head: {
    fontSize: 30,
    marginTop: 58,
    textAlign: "left",
    color: "#001049",
  },
  searchBar: {
    height: 28,
    width: 338,
    marginTop: 10,
    borderWidth: 1,
    flexDirection: "row",
    // paddingRight: 10,
    justifyContent: "flex-start",
    // justifyContent: "center",
    borderColor: " rgba(0, 16, 73, 0.2)",
    // borderWidth: 2,
  },
  searchIcon: {
    marginLeft: 10,
    justifyContent: "center",
    // alignSelf: "center",
  },
  textBar: {
    fontSize: 9,
    marginLeft: 10,
    color: "rgba(0, 16, 73, 0.2)",
    // marginLeft: 35,
    justifyContent: "center",
  },
  tagSmall: {
    width: 95,
    justifyContent: "center",
    height: 25,
    borderRadius: 5,
    marginBottom: 10,
    marginRight: 5,
    backgroundColor: "#00A3FF",
  },
  tagText: {
    textAlign: "center",
    color: "white",
    fontSize: 10,
  },
  navOrder: {
    // width:340,
    // height:140,
    // backgroundColor:"#0097EC",
    fontSize: 19,
    marginTop: 25,
    color: "#001049",
  },
  boxXL: {
    width: 180,
    height: 100,
    marginBottom: 10,
    shadowOpacity: 0.8,
    borderRadius: 10,
    backgroundColor: "white",
    borderRightColor: "#0184CD",
    borderRightWidth: 3,
    marginRight: 15,
    // shadowRadius: 10,
    // backgroundColor: "steelblue",
  },
  boxL: {
    width: 141,
    height: 100,
    borderRadius: 10,
    marginBottom: 10,
    backgroundColor: "#D9372B",
    marginRight: 10,
  },
  boxM1: {
    width: 101,
    height: 75,
    borderRadius: 10,
    marginBottom: 10,
    backgroundColor: "#35C7A4",
    marginRight: 15,
  },
  boxM2: {
    width: 101,
    height: 75,
    borderRadius: 10,
    marginBottom: 10,
    backgroundColor: "#00A3FF",
    marginRight: 15,
  },
  boxM3: {
    width: 101,
    height: 75,
    borderRadius: 10,
    marginBottom: 10,
    backgroundColor: "#EEC227",
    marginRight: 15,
  },
  NavRecent: {
    fontSize: 19,
    color: "#001049",
  },
  activityBox: {
    marginTop: 11,
    width: 340,
    height: 60,
    paddingLeft: 15,
    borderRadius: 10,
    borderRightWidth: 5,
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRightColor: "#00A3FF",
  },
  activityText2: {
    // marginLeft: 15,
    fontSize: 7.5,
    color: "rgba(1, 22, 99, 0.3)",
  },
  activityText: {
    fontSize: 9,
    // marginLeft: 15,
    justifyContent: "center",
    color: "#011663",
  },
  imageCheck: {
    width: 15,
    height: 15,
  },
});
export default dashboard;
