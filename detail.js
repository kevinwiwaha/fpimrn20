import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
const detail = () => {
  return (
    <View style={styles.container}>
      <View style={styles.topBackground}>
        <Text style={styles.head}>Your Route</Text>
      </View>
      <View style={styles.boxXL}></View>
      <Text style={styles.taskText}>Task to do</Text>
      <ScrollView>
        <View style={styles.activityOuter}>
          <View style={styles.activityBox}></View>
          <View style={styles.outerStatus}>
            <View style={styles.boxStatus}></View>
            <View style={styles.boxStatus2}></View>
          </View>
        </View>
        <View style={styles.activityOuter}>
          <View style={styles.activityBox}></View>
          <View style={styles.outerStatus}>
            <View style={styles.boxStatus}></View>
            <View style={styles.boxStatus2}></View>
          </View>
        </View>
        <View style={styles.activityOuter}>
          <View style={styles.activityBox}></View>
          <View style={styles.outerStatus}>
            <View style={styles.boxStatus}></View>
            <View style={styles.boxStatus2}></View>
          </View>
        </View>
        <View style={styles.activityOuter}>
          <View style={styles.activityBox}></View>
          <View style={styles.outerStatus}>
            <View style={styles.boxStatus}></View>
            <View style={styles.boxStatus2}></View>
          </View>
        </View>
        <View style={styles.activityOuter}>
          <View style={styles.activityBox}></View>
          <View style={styles.outerStatus}>
            <View style={styles.boxStatus}></View>
            <View style={styles.boxStatus2}></View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 10,
    // paddingLeft: 20,
    backgroundColor: "#E5E5E5",
  },
  topBackground: {
    backgroundColor: "#36609F",
    height: 250,
    justifyContent: "center",
    // position: "relative",
  },
  head: {
    fontSize: 30,
    color: "#EAEAEA",
    marginLeft: 20,
    marginBottom: 20,
    fontWeight: "bold",
  },
  boxXL: {
    width: 350,
    height: 160,
    marginTop: -80,
    alignSelf: "center",
    marginBottom: 20,
    // shadowOpacity: 0.8,
    borderRadius: 20,
    backgroundColor: "white",
    borderRightColor: "#0184CD",
    borderRightWidth: 5,
  },
  taskText: {
    // width:340,
    // height:140,
    // backgroundColor:"#0097EC",
    fontSize: 19,
    marginTop: 20,
    marginBottom: 15,
    marginLeft: 20,
    color: "#001049",
  },
  activityOuter: {
    width: 350,
    marginLeft: 20,
    height: 80,
    flexDirection: "row",
    marginBottom: 12,
  },
  activityBox: {
    // marginTop: 11,
    width: 305,
    height: 80,
    paddingLeft: 15,
    borderRadius: 10,
    borderRightWidth: 5,
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRightColor: "#00A3FF",
    marginRight: 5,
  },
  outerStatus: {
    flexDirection: "column",
    justifyContent: "space-between",
  },
  boxStatus: {
    width: 45,
    height: 35,
    backgroundColor: "#35C7A4",
    borderRadius: 5,
    // marginBottom: 5,
  },
  boxStatus2: {
    width: 45,
    height: 35,
    backgroundColor: "#EEC227",
    borderRadius: 5,
  },
  activityText2: {
    // marginLeft: 15,
    fontSize: 7.5,
    color: "rgba(1, 22, 99, 0.3)",
  },
  activityText: {
    fontSize: 9,
    // marginLeft: 15,
    justifyContent: "center",
    color: "#011663",
  },
});
export default detail;
