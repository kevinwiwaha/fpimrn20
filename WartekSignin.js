import React, { Component } from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";
import { color } from "react-native-reanimated";

const mainApp = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.register}>WarTek</Text>
      <Text style={styles.registerKecil}>BUILDING FUTURE</Text>
      <View style={styles.plisBisa}>
        <TouchableOpacity style={styles.login}>
          <Text style={styles.dalamDaftar}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.signUp}>
          <Text style={styles.dalamDaftar}>Sign Up</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.or}>or login with</Text>
      {/* <View style={styles.boxLogo}>
        <Text style={styles.logoContainer}>
          facebook <Text style={styles.logoContainer}>google</Text>
        </Text>
      </View> */}
      <View style={styles.box}>
        <TouchableOpacity style={styles.boxFb}>
          <Text style={styles.textBox}>facebook</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.boxGoogle}>
          <Text style={styles.textBox}>google</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    // paddingTop: 10,
    // justifyContent: "center",
    backgroundColor: "#001049",
  },
  textBox: {
    color: "white",
    fontSize: 7,
  },
  register: {
    fontSize: 30,
    marginTop: 258,
    textAlign: "center",
    color: "white",
  },
  registerKecil: {
    fontSize: 12,
    // paddingTop :
    letterSpacing: 7,
    textAlign: "center",
    color: "white",
  },
  img: {
    alignItems: "center",
    marginTop: 63,
  },
  bordertxt: {
    marginLeft: 48,
    marginTop: 40,
  },
  plisBisa: {
    alignItems: "center",
    // paddingBottom: "10",
  },
  box: {
    borderBottomWidth: 1,
    // borderBottomColor: "black",
    flexDirection: "row",
    justifyContent: "space-between",
    // paddingRight: 10,
    // textAlign: "center",
    // backgroundColor: "white",
  },
  login: {
    width: 200,
    height: 40,
    borderRadius: 5,
    backgroundColor: "#00A3FF",
    marginTop: 90,
    justifyContent: "center",
    // flexDirection: "row",
    // justifyContent: "space-between",
    // alignItems: "center",
    // alignContent: "center",
  },
  signUp: {
    width: 200,
    height: 40,
    borderRadius: 5,
    borderColor: "#00A3FF",
    borderWidth: 2,
    marginTop: 10,
    justifyContent: "center",
  },
  dalamDaftar: {
    // alignContent: "center",
    textAlign: "center",
    color: "white",
  },
  teksAtau: {
    color: "#4D4D4D",
    paddingTop: 11,
  },
  masuk: {
    width: 140,
    height: 40,
    borderRadius: 10,
    backgroundColor: "#3EC6FF",
    marginTop: 16,
  },
  or: {
    marginTop: 31,
    fontSize: 7,
    opacity: 30,
    color: "white",
  },
  boxFb: {
    width: 94,
    height: 24,
    backgroundColor: "#36609F",
    marginRight: 5,
    justifyContent: "center",
    borderRadius: 5,
    alignItems: "center",
    // padding: 10,
    // justifyContent: "center",
  },
  boxGoogle: {
    width: 94,
    height: 24,
    justifyContent: "center",
    marginLeft: 5,
    borderRadius: 5,
    alignItems: "center",
    backgroundColor: "#D9372B",
  },
  logoContainer: {
    paddingLeft: 50,
  },
});
export default mainApp;
