import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
const myProfile = () => {
  return (
    <View style={styles.container}>
      <View style={styles.whiteDrop}>
        <Text style={styles.namaNya}>Ahmad Johanes</Text>
        <Text style={styles.kotaNya}>Surabaya</Text>
        <View style={styles.containerFoto}>
          <Image
            style={styles.imej1}
            source={require("./assets/4.jpg")}
            resizeMode={"stretch"}
          />
          <Image
            style={styles.imej2}
            source={require("./assets/3.jpg")}
            resizeMode={"stretch"}
          />
        </View>
        <View style={styles.containerFoto}>
          <Image
            style={styles.imej3}
            source={require("./assets/1.jpg")}
            resizeMode={"stretch"}
          />
          <Image
            style={styles.imej4}
            source={require("./assets/2.jpg")}
            resizeMode={"stretch"}
          />
        </View>
        <View style={styles.prev}>
          <Icon name="arrow-left" size={14} color="#36609F" />
        </View>
      </View>
      <Image
        style={styles.imej}
        source={require("./assets/man.jpg")}
        resizeMode={"stretch"}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#36609F",
  },
  containerFoto: {
    width: 350,
    height: 120,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  whiteDrop: {
    backgroundColor: "#E5E5E5",
    // borderRadius: 64,
    paddingTop: 65,
    width: 394,
    borderTopRightRadius: 64,
    borderTopLeftRadius: 64,
    height: 470,
    position: "absolute",
    bottom: 0,
    alignItems: "center",
  },
  imej: {
    // marginTop: -60,
    width: 210,
    height: 210,
    marginTop: 180,
    borderRadius: 210 / 2,
    alignSelf: "center",
  },
  imej1: {
    // marginTop: -60,
    width: 175,
    height: 117,
    marginTop: 50,
    borderRadius: 210 / 2,
    alignSelf: "center",
  },
  imej2: {
    // marginTop: -60,
    width: 140,
    height: 100,
    marginTop: 50,
    borderRadius: 210 / 2,
    alignSelf: "center",
  },
  imej3: {
    // marginTop: -60,
    width: 170,
    height: 112,
    marginTop: 50,
    borderRadius: 210 / 2,
    alignSelf: "center",
  },
  imej4: {
    // marginTop: -60,
    width: 160,
    height: 112,
    marginTop: 50,
    borderRadius: 210 / 2,
    alignSelf: "center",
  },
  namaNya: {
    fontSize: 24,
    marginBottom: 5,
    color: "#001049",
    fontWeight: "bold",
  },
  kotaNya: {
    fontSize: 14,
    color: "#001049",
  },
  prev: {
    marginTop: 50,
    marginLeft: -240,
  },
});
export default myProfile;
