import React, { Component } from "react";
import { View, Image, StyleSheet, Text } from "react-native";

const mainApp = () => {
  return (
    <View style={styles.container}>
      <View style={styles.bulat}>
        <Image
          style={styles.img}
          source={require("./assets/logo.png")}
          resizeMode={"stretch"}
        />
      </View>
    </View>
  );
};
styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  img: {
    alignItems: "center",
    marginTop: 86,
  },
  bulat: {
    marginTop: 279,
    backgroundColor: "#211F65",
    // opacity: (0, 1),
    height: 305,
    width: 298,
    borderRadius: 150,
    alignItems: "center",
  },
});
export default mainApp;
