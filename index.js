import React, { Component } from "react";
import { Text, View, Image, StyleSheet } from "react-native";

const mainApp = () => {
  return (
    <View>
      {/* <View style={{ width: 80, height: 80, backgroundColor: "#1289A7" }} /> */}
      <Image
        style={styles.img}
        style={styles.container}
        source={require("./assets/logo.png")}
        resizeMode={"stretch"}
      />
      <View style={styles.container}>
        <Text style={styles.register}>Register</Text>
      </View>
      <View style={styles.bordertxt}>
        <Text style={styles.txt}>Username</Text>
        <View style={styles.box}></View>
        <Text style={styles.txt}>Email</Text>
        <View style={styles.box}></View>
        <Text style={styles.txt}>Password</Text>
        <View style={styles.box}></View>
        <Text style={styles.txt}>Ulangi Password</Text>
        <View style={styles.box}></View>
      </View>
      <View style={styles.container}>
        <View style={styles.daftar}>
          <Text style={styles.dalamDaftar}>Daftar</Text>
        </View>
        <Text style={styles.teksAtau}>Atau</Text>
        <View style={styles.masuk}>
          <Text style={styles.dalamDaftar}>Masuk</Text>
        </View>
      </View>
    </View>
  );
};
// const tulisan = () => {
//   return
// }
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  register: {
    fontSize: 24,
    marginTop: 40,
    color: "#003366",
  },
  img: {
    alignItems: "center",
    marginTop: 63,
  },
  bordertxt: {
    marginLeft: 40,
    marginTop: 40,
  },
  txt: {
    fontSize: 16,
    color: "#003366",
  },
  box: {
    width: 294,
    height: 48,
    borderWidth: 1,
    backgroundColor: "white",
  },
  daftar: {
    width: 140,
    height: 40,
    borderRadius: 10,
    backgroundColor: "#003366",
    marginTop: 40,
  },
  dalamDaftar: {
    paddingTop: 6,
    textAlign: "center",
    color: "white",
  },
  teksAtau: {
    color: "#3EC6FF",
    paddingTop: 16,
  },
  masuk: {
    width: 140,
    height: 40,
    borderRadius: 10,
    backgroundColor: "#3EC6FF",
    marginTop: 16,
  },
});
export default mainApp;
